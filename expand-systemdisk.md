# Expand System Disk on Ubuntu Server
> https://packetpushers.net/blog/ubuntu-extend-your-default-lvm-space/

This documentation describes the process for extending a system disk on an Ubuntu Server. Follow the steps below carefully.

## Step 1: Extend disk in Proxmox
1. Open Proxmox.
2. Select the virtual machine that needs more disk space.
3. Expand the disk's capacity to the desired size.

## Step 2: Verify free space
1. Log in to Ubuntu Server.
2. Run the following command to open the Disk Partitioning utility:
 `sudo cfdisk`

Check that there is free space ("Free space") available.

## Step 3: Select disk and resize

Select the disk you want to extend.
Select "Resize" from the menu.
Follow the instructions to extend the partition to cover the free space.

## Step 4: Write changes to disk

Select "Write" from the same menu to write the changes to disk.
Confirm the changes when prompted.

## Step 5: Expand the volume

Run the following command to extend the volume to use all free space:

`lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv`

## Step 6: Resize the file system

Run the following command to resize the file system:

`resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv`

Now the system disk is expanded and ready for use.