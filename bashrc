#!/bin/bash
# bash look
export KUBECONFIG=~/.kube/config
function set_prompt {
    local YELLOW='\[\033[1;33m\]'
    local GREEN='\[\033[1;32m\]'
    local ORANGE='\[\033[1;33m\]'
    local RED='\[\033[1;31m\]'
    local WHITE='\[\033[1;37m\]'
    local BLUE='\[\033[1;34m\]'
    local MAGENTA='\[\033[1;35m\]'
    local RESET='\[\033[0m\]'

    # Hent Kubernetes namespace hvis det er satt
    local k8s_namespace=""
    if [ -n "$KUBECONFIG" ]; then
        k8s_namespace=$(kubectl config view --minify --output 'jsonpath={..namespace}')
    fi

    local namespace_color="${RED}"
    local history_color="${ORANGE}"
    local prompt_namespace=""
    if [ -n "$k8s_namespace" ]; then
        prompt_namespace="${namespace_color}[$k8s_namespace]"
    fi

    PS1="${GREEN}[\u@\h]${BLUE}[\w]${MAGENTA}${prompt_namespace} \n${history_color}[\t - \$(history 1 | sed -e 's/^\s*[0-9]\+\s*//')]\n${RESET}$ "
}

PROMPT_COMMAND=set_prompt
# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Custom aliases
alias lsl='ls -l'
alias lsla='ls -la'
alias upgr='sudo nala update && sudo nala upgrade -y && sudo nala autoremove'
alias c='clear'
alias pip="curl ip.der.se"
alias lip="ip addr show"
# Kubernetes
source /etc/bash_completion 
source <(kubectl completion bash) # set up autocomplete in bash into the current shell, bash-completion package should be installed first.
#echo "source <(kubectl completion bash)"  # add autocomplete permanently to your bash shell.
alias k=kubectl
complete -o default -F __start_kubectl k
alias kns='k config set-context $(k config current-context) --namespace '
alias ksecret='function _mysecret(){ kubectl get secret "$1" -o jsonpath="{.data.$2}" | base64 -d; };_mysecret'
alias kgp='k get pods'
