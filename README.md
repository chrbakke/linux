# Linux

A public repository with purpose to get quick access to common tools and config for your Debian based Linux Desktop environment.

## .bashrc

Before using this, create a backup of your existing file, delete the orignal file and add a link to your new file.

### Create Backup
```bash
cp ~/.bashrc ~/.bashrc.bak
```

### Delete original file
```bash
rm ~/.bashrc
```

### Add link to the file in this repo
> This repo is in ~/git/linux
```bash
ln -n ~/git/linux/bashrc ~/.bashrc
``` 


