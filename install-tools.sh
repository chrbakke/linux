#! /bin/bash

RED='\033[1;33m'
NC='\033[0m' # No Color

echo "Install useful tools from apt repo" 
wait 5
sudo apt update && 

# Install Nala
echo -e "${RED}Install Nala${NC}"
sudo apt install nala -y

# Install Nextcloud client
echo -e "${RED}Install Nextcloud Client${NC}"
sudo apt install nextcloud-desktop -y

# Install Matrix Terminal fun
echo -e "${RED}Install Matrix terminal fun${NC}" 
sudo apt install cmatrix -y

# Install Flameshot
echo -e "${RED}Install Flameshot${NC}" 
sudo apt install flameshot -y

# Install VS Codium
# Add the GPG key of the repository:
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
# Add the repository:
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
# Update then install vscodium (if you want vscodium-insiders, then replace codium by codium-insiders):
echo -e "${RED}Install VS Codium${NC}" 
sudo apt update && sudo apt install codium -y

# Install kubectl
# apt-transport-https may be a dummy package; if so, you can skip that package
sudo apt-get install -y apt-transport-https ca-certificates curl
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
echo -e "${RED}Install kubectl${NC}" 
sudo apt-get install -y kubectl

