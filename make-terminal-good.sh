#! /bin/bash

# Create backup of existing .bashrc file
echo -e "${RED}Creating backup of existing .bashrc file${NC}"
sudo cp ~/.bashrc ~/.bashrc.bak

# Remove existing .bashrc file
echo -e "${RED}Remove existing .bashrc file${NC}"
sudo rm ~/.bashrc

# Create link to new .bashrc file
echo -e "${RED}Creating link to the new .bashrc file${NC}"
ln -n ~/git/linux/bashrc ~/.bashrc

# Load new .bashrc file
echo -e "${RED}Load new .bashrc file${NC}"
source ~/.bashrc